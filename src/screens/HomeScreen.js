import React from 'react';
import { Text, StyleSheet, View, Button } from 'react-native';


const HomeScreen = ({ navigation }) => {
  return (
	  <View>
		<Text style={styles.text}>HomeScreen</Text>
		<Button
			title="Go to components demo"
			onPress={e => navigation.navigate("Components")}
		/>
		<Button
			title="Go to list demo"
			onPress={e => navigation.navigate("List")}
		/>
		<Button
			title="Go to image screen demo"
			onPress={e => navigation.navigate("Image")}
		/>
		<Button
			title="Go to image counter demo"
			onPress={e => navigation.navigate("Counter")}
		/>

		<Button
			title="Go to image colors demo"
			onPress={e => navigation.navigate("Color")}
		/>
		<Button
			title="Go to colored square demo"
			onPress={e => navigation.navigate("Square")}
		/>
		<Button
			title="Go to text screen demo"
			onPress={e => navigation.navigate("Text")}
		/>
		<Button
			title="Go to box screen demo"
			onPress={e => navigation.navigate("Box")}
		/>

	  </View>
	  
  );
};

const styles = StyleSheet.create({
	text: {
		fontSize: 30
	},
});

export default HomeScreen;
