import React, { useReducer } from 'react';
import {
	View, Text, StyleSheet, Button,
} from 'react-native';

const reducer = (state, action) => {
	switch (action.type) {
	case 'INCREMENT':
		return { ...state, counter: state.counter + 1 };
	case 'DECREMENT': 
		return { ...state, counter: state.counter - 1 };
	default:
		return state;
	}
};

const CounterScreen = () => {
	const [state, dispatch] = useReducer(reducer, { counter: 0 });

	return (
		<View>
			<Text>Current count: {state.counter}</Text>
			<Button title="Increase" onPress={e => dispatch({type: 'INCREMENT' })} />
			<Button title="Decrease" onPress={e => dispatch({ type: 'DECREMENT' })} />
		</View>
	);
};

const styles = StyleSheet.create({

});

export default CounterScreen;