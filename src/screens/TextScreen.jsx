import React, { useState } from 'react';
import {
	View, Text, StyleSheet, TextInput,
} from 'react-native';

const TextScreen = () => {
	const [currentText, changeText] = useState('');

	return (
		<View>
			<Text>Enter password</Text>
			<TextInput
				style={styles.input}
				autoCapitalize="none"
				autoCorrect={false}
				value={currentText}
				onChangeText={newValue => changeText(newValue)}
			/>
			{currentText && currentText.length >= 5 
				? null
				: <Text>Passwor needs to be at least 5 characters long</Text>
			}
		</View>
	);
};

const styles = StyleSheet.create({
	input: {
		margin: 15,
		borderColor: 'black',
		borderWidth: 1,
	},
});

export default TextScreen;