import React from 'react';
import {
	View, Text, StyleSheet,
} from 'react-native';

const BoxScreen = () => {
	return (
		// <View style={styles.viewStyle}>
		// 	<Text style={styles.text1Style}>
		// 		Child #1
		// 	</Text>
		// 	<Text style={styles.text2Style}>
		// 		Child #2
		// 	</Text>
		// 	<Text style={styles.text3Style}>
		// 		Child #3
		// 	</Text>
		// </View>
		<View style={styles.container}>
			<View style={styles.box1} />
			<View style={styles.box2} />
			<View style={styles.box3} />
		</View>

	);
};

const styles = StyleSheet.create({
	// viewStyle: {
	// 	// alignItems: 'center',
	// 	// flexDirection: 'row',
	// 	// justifyContent: 'flex-start',
	// 	height: 200,
	// 	borderWidth: 3,
	// 	borderColor: 'black',
	// }, 
	// text1Style: {
	// 	borderWidth: 1,
	// 	borderColor: 'red',
	// 	flex: 4,
	// 	alignSelf: 'flex-start',
	// },
	// text2Style: {
	// 	borderWidth: 1,
	// 	borderColor: 'red',
	// 	flex: 3,
	// 	alignSelf: 'center',
	// 	position: 'absolute',
	// 	// top: 0, // zeros on all four make this elemnt take all the space of container
	// 	// bottom: 0,
	// 	// left: 0,
	// 	// right: 0,
	// 	// same as:
	// 	// ...StyleSheet.absoluteFillObject,
	// },
	// text3Style: {
	// 	borderWidth: 1,
	// 	borderColor: 'red',
	// 	flex: 2,
	// 	alignSelf: 'flex-end',
	// }
	container: {
		alignItems: 'center',
		flexDirection: 'row',
		justifyContent: "space-between",
	},
	box1: {
		height: 100,
		width: 100,
		backgroundColor: 'rgb(15, 100, 56)',
	},
	box2: {
		height: 100,
		width: 100,
		backgroundColor: 'rgb(15, 1, 56)',
		top: 100,
	},
	box3: {
		height: 100,
		width: 100,
		backgroundColor: 'rgb(15, 150, 56)'
	}


});

export default BoxScreen;