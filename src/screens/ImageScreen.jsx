import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import ImageDetail from '../components/ImageDetail';


const ImageScreen = () => {
	return (
		<View>
			<Text>Screen</Text>
			<ImageDetail title="Forest" src={require("../../assets/forest.jpg")} score="9" />
			<ImageDetail title="Beach" src={require("../../assets/beach.jpg")} score="4" />
			<ImageDetail title="Mountain" src={require("../../assets/mountain.jpg")} score="8" />
		</View>
	)
};

const styles = StyleSheet.create({

});

export default ImageScreen;