import React, { useReducer } from 'react';
import { View, StyleSheet } from 'react-native';

import ColorCounter from '../components/ColorCounter';

const COLOR_INCREMENT = 15;

const reducer = (state, action) => {
	switch (action.colorToChange) {
	case 'red':
		return state.red + action.amount > 255 || state.red + action.amount < 0
			? state
			: { ...state, red: state.red + action.amount };
	case 'green':
		return state.green + action.amount > 255 || state.green + action.amount < 0
			? state
			: { ...state, green: state.green + action.amount };
	case 'blue':
		return state.blue + action.amount > 255 || state.blue + action.amount < 0
			? state
			: { ...state, blue: state.blue + action.amount };
	default:
		return;
	}
};

const SquareScreen = () => {
	const initialState = {
		red: 0, 
		green: 0,
		blue: 0,
	}; 

	const [state, dispatch] = useReducer(reducer, initialState);
	const { red, green, blue } = state;

	return (
		<View>
			<ColorCounter color="red"
				onIcrease={() => dispatch({ colorToChange: 'red', amount: COLOR_INCREMENT })}
				onDecrease={() => dispatch({ colorToChange: 'red', amount:  -1 * COLOR_INCREMENT })}
			/>
			<ColorCounter
				color="green"
				onIcrease={() => dispatch({ colorToChange: 'green', amount: COLOR_INCREMENT })}
				onDecrease={() => dispatch({ colorToChange: 'green', amount: -1 * COLOR_INCREMENT })}
/>
			<ColorCounter
				color="blue"
				onIcrease={() => dispatch({ colorToChange: 'blue', amount: COLOR_INCREMENT })}
				onDecrease={() => dispatch({ colorToChange: 'blue', amount: -1 * COLOR_INCREMENT })}
				// by convension action creator:
				// dispatch({ type: 'change_blue', payload: -1 * COLOR_INCREMENT})
			/>
			<View style={{ height: 150, width: 150, backgroundColor: `rgb(${red}, ${green}, ${blue})` }} />
		</View>
	);
};

const styles = StyleSheet.create({

});

export default SquareScreen;