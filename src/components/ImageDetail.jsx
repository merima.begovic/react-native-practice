import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

const ImageDetail = ({ title, src, score }) => {
	return (
		<View>
			<Text>{title}</Text>
			<Image source={src} />
			<Text>
				score: {score}
			</Text>
		</View>
	)
};

const styles = StyleSheet.create({

});

export default ImageDetail;