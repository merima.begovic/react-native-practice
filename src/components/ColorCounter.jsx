import React from 'react';
import { View, StyleSheet, Button, Text } from 'react-native';

const ColorCounter = ({ color, onIcrease, onDecrease }) => {
	return (
		<View>
			<Text>{color}</Text>
			<Button title={`More ${color}`} 
				onPress={() => onIcrease()}
			/>
			<Button title={`Less ${color}`} 
				onPress={() => onDecrease()}
			/>
		</View>
	);
};

const styles = StyleSheet.create({

});

export default ColorCounter;